import { StatusBar } from 'expo-status-bar'
import React, { FunctionComponent } from 'react'
import styled from 'styled-components/native'

// custom components
import { colors } from '../components/colors'
import { Container } from '../components/shared'

const HomeContainer = styled(Container)`
    background-color: ${colors.grayLight};
    flex: 1;
    width: 100%;

`

const TopSection = styled(Container)`
    background-color: ${colors.secondary};
    width: 100%;
    flex: 1;
    max-height: 55%;
`

const TopImage = styled.Image`
    width: 100%;
    height: 100%;
    resize-mode: stretch;
`

const BottomSection = styled(Container)`
    background-color: ${colors.secondary};
    width: 100%;
    padding: 25px;
    flex: 1;
    justify-content: flex-end;
`

import background from "./../assets/bgs/background_v1.png"
import BigText from '../components/Texts/BigText'
import SmallText from '../components/Texts/SmallText'
import RegularButton from '../components/Buttons/RegularButton'

import CardSection from '../components/Cards/CardSection'
import TransactionSection from '../components/Transactions/TransactionSection'
import SendMoneySection from '../components/SendMoney/SendMoneySection'

import visaLogo from "../assets/images/visa-2.png"
import mastercardLogo from "../assets/images/master-card.png"

import portrait1 from "../assets/portraits/1.jpg"
import portrait2 from "../assets/portraits/2.jpg"
import portrait3 from "../assets/portraits/3.jpg"

const Home: FunctionComponent = () => {
  const cardsData = [
      {
          id: 1,
          accountNo: "384598681",
          balance: 20000.15,
          alias: "Work Debit",
          logo: visaLogo,
      },
      {
        id: 2,
        accountNo: "168168168",
        balance: 12000.01,
        alias: "Personal Prepaid",
        logo: mastercardLogo,
      },
      {
        id: 3,
        accountNo: "111185125",
        balance: 5600.83,
        alias: "School Prepaid",
        logo: visaLogo,
      }
  ]

  const transactionData = [
      {
          id: 1,
          amount: "-$86.00",
          date: "14 Sep 2021",
          title: "Taxi",
          subtitle: "Uber car",
          art: {
              background: colors.primary,
              icon: "car",
          }
      },
      {
        id: 2,
        amount: "-$286.00",
        date: "15 Sep 2021",
        title: "Shopping",
        subtitle: "Ali express",
        art: {
            background: colors.tertiary,
            icon: "car",
        }
      },
      {
        id: 3,
        amount: "-$26.00",
        date: "16 Sep 2021",
        title: "Bus",
        subtitle: "Public Transpor",
        art: {
            background: colors.tertiary,
            icon: "car",
        }
      },
  ]

  const sendMoneyData = [
      {
          id: 1,
          amount: "2450.56",
          name: "Coby Andoh",
          background: colors.tertiary,
          img: portrait1,
      },
      {
        id: 2,
        amount: "4450.56",
        name: "Harley Queen",
        background: colors.primary,
        img: portrait2,
      },
      {
        id: 3,
        amount: "6250.56",
        name: "James Corbyn",
        background: colors.accent,
        img: portrait3,
      },
  ]

  return (
    <HomeContainer>
        <StatusBar style='dark' />
        <CardSection data={cardsData} />
        <TransactionSection data={transactionData} />
        <SendMoneySection data={sendMoneyData}/>
    </HomeContainer>
  )
}

export default Home