import { StyleSheet, Text, View } from 'react-native'
import { StatusBar } from 'expo-status-bar'
import React, { FunctionComponent } from 'react'
import styled from 'styled-components/native'

// custom components
import { colors } from '../components/colors'
import { Container } from '../components/shared'

const WelcomeContainer = styled(Container)`
    background-color: ${colors.secondary};
    justify-content: space-between;
    width: 100%;
    height: 100%;
`

const TopSection = styled(Container)`
    background-color: ${colors.secondary};
    width: 100%;
    flex: 1;
    max-height: 55%;
`

const TopImage = styled.Image`
    width: 100%;
    height: 100%;
    resize-mode: stretch;
`

const BottomSection = styled(Container)`
    background-color: ${colors.secondary};
    width: 100%;
    padding: 25px;
    flex: 1;
    justify-content: flex-end;
`

import background from "./../assets/bgs/background_v1.png"
import BigText from '../components/Texts/BigText'
import SmallText from '../components/Texts/SmallText'
import RegularButton from '../components/Buttons/RegularButton'

const Welcome: FunctionComponent = () => {
  return (
    <>
        <StatusBar style='light' />
        <WelcomeContainer>
            <TopSection>
                <TopImage source={background} />
            </TopSection>
            <BottomSection>
                <BigText textStyles={{
                    width: "70%",
                    marginBottom: 25,
                    marginLeft: -75
                }}>
                    Best way to track your money
                </BigText>
                <SmallText textStyles={{
                    width: "70%",
                    marginBottom: 25,
                    marginLeft: -75
                }}>
                    Best payment method, connect your money to your friends, family
                </SmallText>
                <RegularButton onPress={() => {}}>
                    Get Started
                </RegularButton>
            </BottomSection>
        </WelcomeContainer>
    </>
  )
}

export default Welcome

const styles = StyleSheet.create({})